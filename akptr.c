#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <linux/input-event-codes.h>
#include <stdbool.h>
#include <signal.h>

#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>

volatile sig_atomic_t interrupted = false;

struct keyboard {
    struct libevdev *dev;
    int fd;
};

struct pointer {
    struct libevdev_uinput *uidev;
    struct libevdev *dev;
    int fd;
};

void sigint_handler(int signum) {
    interrupted = true;
}

int main(int argc, char* argv[]) {
    struct sigaction action;
    struct keyboard kbd;
    struct pointer ptr;
    int ret;

    action.sa_handler = sigint_handler;
    sigemptyset (&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGINT, &action, NULL);

    kbd.fd = open("/dev/input/by-id/usb-Wooting_WootingTwo_WOOT_000_A01B1932W021H00062-if05-event-joystick", O_RDONLY);
    if (kbd.fd < 0) {
        fprintf(stderr, "Failed to open analog keyboard event device: %s\n",
                strerror(errno));
        exit(1);
    }

    ret = libevdev_new_from_fd(kbd.fd, &kbd.dev);
    if (ret < 0) {
        fprintf(stderr, "Failed to initialize keyboard device: %s\n",
                strerror(-ret));
        exit(1);
    }

    // TODO(PM): Create a virtual mouse without duplicating an existing device
    ptr.fd = open("/dev/input/event3", O_RDONLY);
    if (ptr.fd < 0) {
        fprintf(stderr, "Failed to open mouse event device: %s\n",
                strerror(errno));
        exit(1);
    }

    ret = libevdev_new_from_fd(ptr.fd, &ptr.dev);
    if (ret < 0) {
        fprintf(stderr, "Failed to initialize virtual mouse: %s\n",
                strerror(-ret));
        exit(1);
    }

    ret = libevdev_uinput_create_from_device(ptr.dev, LIBEVDEV_UINPUT_OPEN_MANAGED,
                                             &ptr.uidev);
    if (ret != 0) {
        fprintf(stderr, "Failed to initialize uinput device\n");
        exit(1);
    }

    while(!interrupted) {
        struct input_event ev;

        ret = libevdev_next_event(kbd.dev, LIBEVDEV_READ_FLAG_NORMAL, &ev);
        if (ret == 0 && ev.type == EV_ABS) {
            fprintf(stdout, "<< %s %s %d\n",
                    libevdev_event_type_get_name(ev.type),
                    libevdev_event_code_get_name(ev.type, ev.code),
                    ev.value);

            fprintf(stdout, ">> %s %s %d\n",
                libevdev_event_type_get_name(EV_REL),
                libevdev_event_code_get_name(EV_REL, ev.code),
                ev.value >> 11);
            libevdev_uinput_write_event(ptr.uidev, EV_REL, ev.code,
                                        ev.value >> 11);
            libevdev_uinput_write_event(ptr.uidev, EV_SYN, SYN_REPORT, 0);
        }
    }

    libevdev_uinput_destroy(ptr.uidev);
    libevdev_free(ptr.dev);
    close(ptr.fd);
    libevdev_free(kbd.dev);
    close(kbd.fd);
    exit(0);
}
